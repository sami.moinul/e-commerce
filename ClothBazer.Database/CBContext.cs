﻿
using ClothBazer.Entities;
using System.Data.Entity;


namespace ClothBazer.Database
{
    public class CBContext : DbContext
    {
        public CBContext() : base("ClothBazerConnection")
        {

        }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Product>  Products { get; set; }
    }
}
